import { Global, Module, OnModuleInit } from '@nestjs/common';
import { log } from '../util/als-logger.util';
import { AlsLogger } from './als-logger.service';

@Global()
@Module({
  providers: [AlsLogger],
  exports: [AlsLogger],
})
export class LoggerModule implements OnModuleInit {
  onModuleInit() {
    log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);
  }
}
