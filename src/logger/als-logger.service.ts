/* istanbul ignore file */
import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { AlsMessage, logger } from '../util/als-logger.util';

const DUMMY_REQUEST_ID = '00000000-0000-0000-0000-000000000000';

type MyRequest = {
  apiGateway?: {
    event: {
      requestContext: {
        authorizer: { isAuthorized?: string };
        requestId: string;
      };
    };
  };
};

@Injectable({ scope: Scope.REQUEST })
export class AlsLogger {
  private requestId: string;

  constructor(@Inject(REQUEST) private request: MyRequest) {
    this.requestId = this.request.apiGateway?.event.requestContext.requestId ?? DUMMY_REQUEST_ID;
  }

  error(message: AlsMessage | string) {
    logger.error(this.enrichMessage(message));
  }

  log(...msg: any[]) {
    if (typeof msg === 'string') {
      logger.info(this.enrichMessage(msg));
    } else {
      logger.info(this.enrichMessage(JSON.stringify(msg)));
    }
  }

  info(message: AlsMessage | string) {
    logger.info(this.enrichMessage(message));
  }

  debug(message: AlsMessage | string) {
    logger.debug(this.enrichMessage(message));
  }

  trace(message: AlsMessage | string) {
    logger.trace(this.enrichMessage(message));
  }

  warn(message: AlsMessage | string) {
    logger.warn(this.enrichMessage(message));
  }

  private enrichMessage(message: AlsMessage | string): AlsMessage {
    if (typeof message === 'string') {
      return { description: message, correlationId: this.requestId };
    } else {
      return { ...message, correlationId: this.requestId };
    }
  }
}
