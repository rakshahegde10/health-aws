import { IsUUID, IsAlpha, IsNumberString, IsDateString, Matches, ValidateNested, ArrayNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';

class MeasurementItem {
  @IsAlpha()
  attributeName: string;
  @IsNumberString()
  attributeValue: string;
  @Matches(/^[a-zA-Z0-9/]{1,10}$/, { message: 'invalid attribute unit' })
  attributeUnit: string;
}

class Measurement {
  @IsUUID()
  measurementId: string;
  source: string;
  measurementType: string;
  @IsDateString()
  measurementDatetime: string;
  @ValidateNested({ each: true })
  @Type( () => MeasurementItem )
  @ArrayNotEmpty()
  measurementItems: MeasurementItem[];
}

class MeasurementSet {
  @Matches(/^[a-zA-Z0-9_-]{3,100}$/, { message: 'userId is invalid' })
  userId: string;
  undercareUserId: string | null;
  undercareUserRole: string | null;
  @ValidateNested({ each: true })
  @Type( () => Measurement )
  @ArrayNotEmpty()
  measurements: Measurement[];
  @IsDateString()
  submissionDatetime: string;
}

export class MeasurementCreateInput {
  content: string;
  en_key: string;
  @ValidateNested()
  @Type( () => MeasurementSet )
  data: MeasurementSet;
}

export class MeasurementQueryInput {
  @Matches(/^[a-zA-Z0-9_-]{3,50}$/, { message: 'userId is invalid' })
  userId: string;
  @Matches(/^[a-zA-Z0-9_-]{22,26}$/, { message: 'startDate is invalid' })
  startDate: string;
  @Matches(/^[a-zA-Z0-9_-]{22,26}$/, { message: 'endDate is invalid' })
  endDate: string;
  @Matches(/^[a-zA-Z0-9_-]{2,50}$/, { message: 'type is invalid' })
  type: string;
  content: string;
  en_key: string;
}

export class MeasurementModificationInput {
  @Matches(/^[a-zA-Z0-9_-]{3,50}$/, { message: 'userId is invalid' })
  userId: string;
  @Matches(/^[a-zA-Z0-9_-]{20,100}$/, { message: 'recordKey is invalid' })
  recordKey: string;
  content: string;
  en_key: string;
}


