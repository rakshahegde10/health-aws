export type CheckTokenResult = {
  hago_user_role: string;
  undercare_user_id: string;
  user_id: string;
  device_uuid: string;
};

export type SuccessResult = {
  success: boolean;
};
