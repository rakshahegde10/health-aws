import { Body, Controller, Delete, Post, HttpCode } from '@nestjs/common';
import { MeasurementService } from '../service/measurement.service';
import {
  MeasurementCreateInput,
  MeasurementQueryInput,
  MeasurementModificationInput,
} from '../input/measurement.input';
import { checkHagoUserToken } from '../../util/hago-auth.util';
import { HttpException } from '@nestjs/common';
import 'source-map-support/register';

@Controller('v1/measurement')
export class MeasurementController {
  constructor(private readonly measurementService: MeasurementService) {}

  @Post()
  async create(@Body() input: MeasurementCreateInput) {
    if (!(await checkHagoUserToken(input.data.userId, input.content, input.en_key))) {
      return new HttpException('error in check Hago User Token ', 400);
    }
    return this.measurementService.create(input);
  }

  @Post('/get')
  @HttpCode(200)
  async get(@Body() input: MeasurementQueryInput) {
    if (!(await checkHagoUserToken(input.userId, input.content, input.en_key))) {
      return new HttpException('error in check Hago User Token ', 400);
    }
    return this.measurementService.get(input);
  }

  @Delete('/delete')
  async delete(@Body() input: MeasurementModificationInput) {
    if (!(await checkHagoUserToken(input.userId, input.content, input.en_key))) {
      return new HttpException('error in check Hago User Token ', 400);
    }
    return this.measurementService.delete(input);
  }

  @Post('/test')
  async createTest(@Body() input: MeasurementCreateInput) {
    return this.measurementService.create(input);
  }

  @Post('/test/get')
  @HttpCode(200)
  async getByPassAuth(@Body() input: MeasurementQueryInput) {
    return this.measurementService.get(input);
  }

  @Delete('/test/delete')
  async deleteByPassAuth(@Body() input: MeasurementModificationInput) {
    return this.measurementService.delete(input);
  }
}
