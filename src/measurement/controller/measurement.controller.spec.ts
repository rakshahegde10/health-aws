import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
//import { buildKeyPath } from '../../util/helper.util';
import { MeasurementService } from '../service/measurement.service';
import { MeasurementTestImports } from '../test/measurement-test.imports';
import { MeasurementController } from './measurement.controller';

jest.mock('../../client/vsg-svc.client');

let app: INestApplication;

beforeAll(async () => {
  const module: TestingModule = await Test.createTestingModule({
    imports: MeasurementTestImports,
    providers: [MeasurementService],
    controllers: [MeasurementController],
  }).compile();

  app = module.createNestApplication(undefined, { logger: [] });
  app.useGlobalPipes(new ValidationPipe({ forbidUnknownValues: true }));
  await app.init();
});

describe('Mesasurement Controller', () => {
  beforeAll(async () => {
    expect(app).toBeDefined();
  });

  it('Put Data - success', () => {
    return request(app.getHttpServer())
      .post('/measurement')
      .send({
        userId: 'rn00000001',
        data: 'abcdXXXX',
      })
      .expect(201, { success: true });
  });

  it('Put Data - userId invalid', () => {
    return request(app.getHttpServer())
      .post('/measurement')
      .send({ userId: 'asd', data: 'some data' })
      .expect(400, {
        statusCode: 400,
        message: ['userId is invalid'],
        error: 'Bad Request',
      });
  });

  it('Put Data - missing userId', () => {
    return request(app.getHttpServer())
      .post('/measurement')
      .send({ data: 'some data' })
      .expect(400, {
        statusCode: 400,
        message: ['userId is invalid'],
        error: 'Bad Request',
      });
  });

  it('Put Data - missing data', () => {
    return request(app.getHttpServer())
      .post('/measurement')
      .send({ data: 'some data' })
      .expect(400, {
        statusCode: 400,
        message: ['userId is invalid'],
        error: 'Bad Request',
      });
  });
});
