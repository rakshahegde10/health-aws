import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from '../../logger/logger.module';

export const MeasurementTestImports = [ConfigModule.forRoot(), LoggerModule];
