import { Module } from '@nestjs/common';
import { MeasurementController } from './controller/measurement.controller';
import { MeasurementService } from './service/measurement.service';

@Module({
  providers: [MeasurementService],
  controllers: [MeasurementController],
})
export class MeasurementModule {}
