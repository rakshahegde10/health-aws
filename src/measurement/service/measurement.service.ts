import { Injectable } from '@nestjs/common';
import { AlsLogger } from '../../logger/als-logger.service';
import {
  MeasurementCreateInput,
  MeasurementModificationInput,
  MeasurementQueryInput,
} from '../input/measurement.input';
//import { CheckTokenResult } from '../measurement.type';
import { Kinesis } from 'aws-sdk';
import fetch from 'node-fetch';
import { HttpException } from '@nestjs/common';
import { logger } from '../../util/als-logger.util';

const kinesis = new Kinesis();
@Injectable()
export class MeasurementService {
  constructor(private readonly logger: AlsLogger) {}
  async create(input: MeasurementCreateInput) {
    const params = {
      Records: [
        {
          Data: JSON.stringify(input.data),
          PartitionKey: input.data.userId,
        },
      ],
      StreamName: `${process.env.SERVICE}-${process.env.STAGE}-hago-stream`,
    };

    const result = await kinesis.putRecords(params).promise();
    this.logger.log('MeasurementService putRecords result', result);
    return JSON.stringify({ success: !result.FailedRecordCount });
  }

  async delete(input: MeasurementModificationInput) {
    this.request('DELETE', 'measurement', input);
  }

  async get(input: MeasurementQueryInput) {
    this.request('POST', 'measurement', input);
  }

  async request(method: string, path: string, body: MeasurementModificationInput | MeasurementQueryInput) {
    const AUTH_KEY = Buffer.from(
      process.env.UPLOAD_CLIENT_ID?.trim() + ':' + process.env.UPLOAD_CLEINT_SECRET?.trim(),
    ).toString('base64');

    const res = await fetch(`${process.env.HAGO_CHECKTOKEN_URL?.trim()}/${path}`, {
      method,
      headers: {
        'Content-Type': 'application/json',
        'Keep-Alive': 'true',
        'x-gateway-apikey': process.env.UPLOAD_API_KEY?.trim() || '',
        Authorization: 'Basic ' + AUTH_KEY,
      },
      ...(body ? { body: JSON.stringify(body) } : {}),
      timeout: 30000,
    });

    if (res.status === 404) {
      return undefined;
    } else if (res.status === 400) {
      return (await res.json()).message;
    } else if (res.status >= 300) {
      const text = await res.text();
      logger.error(`message: ${text} status: ${res.status}`);
      throw new HttpException(text, res.status);
    }
    return res.json();
  }
}
