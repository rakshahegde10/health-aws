/* istanbul ignore file */

import { HttpException } from '@nestjs/common';
import fetch from 'node-fetch';
import { Record } from '../stream-handler/type/record';
import { SuccessResult } from '../measurement/measurement.type';
import 'source-map-support/register';
import { logger } from '../util/als-logger.util';

export function sendRecord(records: Record[]): Promise<SuccessResult | string[] | undefined> {
  return request('POST', 'v2/record', records);
}

/*const AUTH_KEY = Buffer.from(
  process.env.UPLOAD_CLIENT_ID?.trim() + ':' + process.env.UPLOAD_CLEINT_SECRET?.trim(),
).toString('base64');*/

async function request(method: string, path: string, body: any) {
  //console.log(method, 'to', `${process.env.UPLOAD_URL?.trim()}/${path}`, 'with body', JSON.stringify(body));
  const res = await fetch(`${process.env.UPLOAD_URL?.trim()}/${path}`, {
    method,
    headers: {
      'Content-Type': 'application/json',
      //'x-gateway-apikey': process.env.UPLOAD_API_KEY?.trim() || '',
      //Authorization: 'Basic ' + AUTH_KEY,
    },
    ...(body ? { body: JSON.stringify(body) } : {}),
    timeout: 30000,
  });

  if (res.status === 404) {
    return undefined;
  } else if (res.status === 400) {
    return (await res.json()).message;
  } else if (res.status >= 300) {
    const text = await res.text();
    logger.error(`message: ${text} status: ${res.status}`);
    throw new HttpException(text, res.status);
  }
  return res.json();
}
