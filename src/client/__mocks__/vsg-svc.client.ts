import { HttpException } from '@nestjs/common';
import { Record } from '../../stream-handler/type/record';

export const sendRecords = async (records: Record[]) => {
  if (records.length > 1) {
    return ['multi records is not supported by HA in phase 1'];
  }
  switch (records[0].userId) {
    case 'HN000200':
      return { success: true };
    case 'HN000404':
      return undefined; //status code 404
    case 'HN000400':
      return ['Incorrect data format', 'Invalid data']; //status code 400
    case 'HN000501':
      throw new HttpException('simulate some other errors', 300); //status code > 300
    default:
      return { success: true };
  }
};

export const getWardList = async (hospCode: string) => {
  switch (hospCode) {
    case 'VHCDABC':
      return undefined; //status code 404
    case 'AV:H':
      return ['Invalid hospCode'];
    default:
      return {
        responseCode: 0,
        responseMessage: null,
        wardList: [
          {
            activeStatus: 'A',
            careCategory: 'R',
            code: '000 ',
            defaultSpecialty: null,
            description: 'tsea 12141234',
            effectiveDate: '01 Feb 2018 00:00',
            hospital: 'TMH',
            location: null,
            treatmentLocation: 'PRI ',
            userDefine: 'Y',
            wristbandNo: 1,
          },
          {
            activeStatus: 'A',
            careCategory: 'U',
            code: '00AA',
            defaultSpecialty: null,
            description: 'test',
            effectiveDate: '01 Jan 2019 00:00',
            hospital: 'TMH',
            location: 'TEST',
            treatmentLocation: 'A&E ',
            userDefine: 'Y',
            wristbandNo: 1,
          },
          {
            activeStatus: 'A',
            careCategory: 'A',
            code: '1   ',
            defaultSpecialty: null,
            description: 'test',
            effectiveDate: '19 Dec 2019 00:00',
            hospital: 'TMH',
            location: null,
            treatmentLocation: null,
            userDefine: 'Y',
            wristbandNo: 0,
          },
        ],
      };
  }
};
