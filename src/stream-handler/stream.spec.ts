process.env.SERVICE = 'myhealth-iot-svc';
process.env.LOG_LEVEL = 'error';
process.env.STAGE = 'dev';
process.env.DLQ_URL = 'https://localhost.some.path.com/303498446923/some-dlq-queue';

import { KinesisStreamEvent } from 'aws-lambda';
//import { PatientProfileSchema } from '../schema/patient-profile.schema';
import AWS from 'aws-sdk';
import * as dynamoose from 'dynamoose';
import { handler } from './stream';
import { HttpException } from '@nestjs/common';
import * as AWSMock from 'jest-aws-sdk-mock';

const vsgClient = require('../client/vsg-svc.client');
jest.mock('../client/vsg-svc.client');
const sendRecords = jest.spyOn(vsgClient, 'sendRecords');

AWS.config.update({ region: 'localhost' });
dynamoose.aws.ddb.local('http://localhost:8001');

let DLQ: any = {};
AWSMock.setSDKInstance(AWS);
AWSMock.mock('SQS', 'sendMessage', (params: any, callback: Function) => {
  DLQ = params;
  callback();
});

describe('stream.ts', function () {
  beforeAll(async () => {
    //await Profile.batchPut(dbDummyRecord);
  });

  afterEach(() => {
    vsgClient.sendRecords.mockClear();
    DLQ.MessageBody = undefined;
    eventExample.Records[0].eventSourceARN =
      'arn:aws:kinesis:ap-east-1:641128055721:stream/kinesis-lab2-dev-hkt-stream';
  });

  it('should send HA correct db record, success', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:KCH:rn0000200';
    await handler(eventExample, contextExample, () => {});
    expect(DLQ.MessageBody).toEqual(undefined);
    expect(vsgClient.sendRecords.mock.calls[0][0][0]).toEqual({
      data: 'some FHIR data',
      vendorCode: 'HKT',
      locationCode: 'KCH',
      caseNum: 'HN000200',
    });
  });

  it('should write to DLQ when stream name not match resources is wrong', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKTABC:AWE:rn00000004';
    await handler(eventExample, contextExample, () => {});
    expect(sendRecords).not.toHaveBeenCalled();
    expect(DLQ.MessageBody).toContain('vendorCode not match stream name');
  });

  it('should send HA correct db record, success (Quick Win)', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:AWE:rn0000200';
    await handler(eventExample, contextExample, () => {});
    expect(DLQ.MessageBody).toEqual(undefined);
    expect(vsgClient.sendRecords.mock.calls[0][0][0]).toEqual({
      data: 'some FHIR data',
      vendorCode: 'HKT',
      hospCode: 'AWE',
      locationCode: 'AWE',
      caseNum: 'HN002200',
    });
  });

  it('can find old patient profile even after AWE/LYM upgraded their devices(QUICK WIN)', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:NLT:rn0000200';
    await handler(eventExample, contextExample, () => {});
    expect(DLQ.MessageBody).toEqual(undefined);
    expect(vsgClient.sendRecords.mock.calls[0][0][0]).toEqual({
      data: 'some FHIR data',
      vendorCode: 'HKT',
      hospCode: 'AWE',
      locationCode: 'AWE',
      caseNum: 'HN002200',
    });
  });

  it('should get registration from HA when no data in DB, success (Quick Win)', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:AWE:rn00000002';
    await handler(eventExample, contextExample, () => {});
    expect(DLQ.MessageBody).toEqual(undefined);
    expect(vsgClient.sendRecords.mock.calls[0][0][0]).toEqual({
      data: 'some FHIR data',
      vendorCode: 'HKT',
      locationCode: 'AWE',
      hospCode: 'AWE',
      caseNum: 'HN00000002',
    });
  });

  it('should write to DLQ when invalid result return from registration (Quick Win)', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:AWE:rn00000006';
    await handler(eventExample, contextExample, () => {});
    expect(sendRecords).not.toHaveBeenCalled();
    expect(DLQ.MessageBody).toContain('get Registration unknown return:');
  });

  it('should write to DLQ when paitent not find during registration (Quick Win)', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:AWE:rn0000904';
    await handler(eventExample, contextExample, () => {});
    expect(sendRecords).not.toHaveBeenCalled();
    expect(DLQ.MessageBody).toEqual(
      JSON.stringify({
        records: [
          {
            _message: 'Registration not found',
            vendorCode: 'HKT',
            locationCode: 'AWE',
            userId: 'rn0000904',
            data: 'some FHIR data',
          },
        ],
        logGroupName: '/aws/lambda/kinesis-lab2-dev-hkt-stream-function',
        logStreamName: '2020/12/01/[$LATEST]86193a4b4052420fa34638e442b6a8d5',
        requestId: 'e274cd43-b165-4473-ab75-6ce3ec72bb0b',
      }),
    );
  });

  it('should write to DLQ when registration return error (Quick Win)', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:AWE:rn00000004';
    await handler(eventExample, contextExample, () => {});
    expect(sendRecords).not.toHaveBeenCalled();
    expect(DLQ.MessageBody).toEqual(
      JSON.stringify({
        records: [
          {
            _message: ['vendorCode is invalid', 'locationCode is invalid', 'userId is invalid'],
            vendorCode: 'HKT',
            locationCode: 'AWE',
            userId: 'rn00000004',
            data: 'some FHIR data',
          },
        ],
        logGroupName: '/aws/lambda/kinesis-lab2-dev-hkt-stream-function',
        logStreamName: '2020/12/01/[$LATEST]86193a4b4052420fa34638e442b6a8d5',
        requestId: 'e274cd43-b165-4473-ab75-6ce3ec72bb0b',
      }),
    );
  });

  it('should write to DLQ, when HA say invalid record during sendRecords', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:KCH:rn0000400';
    await handler(eventExample, contextExample, () => {});
    expect(sendRecords).toHaveBeenCalledTimes(1);
    expect(DLQ.MessageBody).toContain('sendRecords reponse: [\\"Incorrect data format\\",\\"Invalid data\\"]"');
  });

  it('should write to DLQ when HA server not found during sendRecords', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:KCH:rn0000404';
    await handler(eventExample, contextExample, () => {});
    expect(sendRecords).toHaveBeenCalledTimes(1);
    expect(DLQ.MessageBody).toContain(
      'sendRecords reponse: undefined","vendorCode":"HKT","locationCode":"KCH","caseNum":"HN000404"',
    );
  });

  it('should throw exception when HA server error', async () => {
    eventExample.Records[0].kinesis.partitionKey = 'HKT:KCH:rn0000501';
    try {
      await handler(eventExample, contextExample, () => {});
      expect('Http Exception').toBe('throw');
    } catch (e) {
      expect(e instanceof HttpException).toBe(true);
      expect(e.message).toBe('simulate some other errors');
    }
    expect(sendRecords).toHaveBeenCalledTimes(1);
    expect(DLQ.MessageBody).toBeUndefined();
  });
});

let eventExample: KinesisStreamEvent = {
  Records: [
    {
      kinesis: {
        kinesisSchemaVersion: '1.0',
        partitionKey: 'HKT:KCH:rn00000002',
        sequenceNumber: '49613184552370129990023814466109544564964591315179274242',
        data: 'c29tZSBGSElSIGRhdGE=',
        approximateArrivalTimestamp: 1606808802.979,
      },
      eventSource: 'aws:kinesis',
      eventVersion: '1.0',
      eventID: 'shardId-000000000000:49613184552370129990023814466109544564964591315179274242',
      eventName: 'aws:kinesis:record',
      invokeIdentityArn: 'arn:aws:iam::641128055721:role/kinesis-lab2-dev-ap-east-1-lambdaRole',
      awsRegion: 'ap-east-1',
      eventSourceARN: 'arn:aws:kinesis:ap-east-1:641128055721:stream/kinesis-lab2-dev-hkt-stream',
    },
  ],
};

let contextExample = {
  callbackWaitsForEmptyEventLoop: true,
  functionVersion: '$LATEST',
  functionName: 'kinesis-lab2-dev-hkt-stream-function',
  memoryLimitInMB: '512',
  logGroupName: '/aws/lambda/kinesis-lab2-dev-hkt-stream-function',
  logStreamName: '2020/12/01/[$LATEST]86193a4b4052420fa34638e442b6a8d5',
  invokedFunctionArn: 'arn:aws:lambda:ap-east-1:641128055721:function:kinesis-lab2-dev-hkt-stream-function',
  awsRequestId: 'e274cd43-b165-4473-ab75-6ce3ec72bb0b',
  getRemainingTimeInMillis: () => 123,
  done: () => {},
  fail: () => {},
  succeed: () => {},
};
