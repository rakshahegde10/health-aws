export type KinesisStreamInvocationRecord = {
  requestContext: {
    requestId: string;
    functionArn: string;
    condition: string;
    approximateInvokeCount: number;
  };
  responseContext: {
    statusCode: number;
    executedVersion: string;
    functionError: string;
  };
  version: string;
  timestamp: string;
  KinesisBatchInfo: KinesisBatchInfo;
};

export type KinesisBatchInfo = {
  shardId: string;
  startSequenceNumber: string;
  endSequenceNumber: string;
  approximateArrivalOfFirstRecord: string;
  approximateArrivalOfLastRecord: string;
  batchSize: number;
  streamArn: string;
};
