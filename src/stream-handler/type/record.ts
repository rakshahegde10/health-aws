export type Record = {
  userId: string;
  data: string;
  _message?: string | string[];
};
