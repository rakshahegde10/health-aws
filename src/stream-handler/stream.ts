import { KinesisStreamHandler } from 'aws-lambda';
import { SQS } from 'aws-sdk';
import { sendRecord } from '../client/vpc-my-health-sb.client';
import { log, logger } from '../util/als-logger.util';
import { Record } from './type/record';
import 'source-map-support/register';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);
const sqs = new SQS({ apiVersion: '2012-11-05' });
export const load_test_handler: KinesisStreamHandler = async () => {
  //do not waste backend resources when doing kinesis load test
  return;
};
const LambdaId = Math.floor(Math.random() * 100000);

export const handler: KinesisStreamHandler = async (event, context) => {
  if (!process.env.DLQ_URL) {
    logger.error('DLQ not set in env!');
    return;
  }
  if (!process.env.MSG_BATCH_SIZE) {
    logger.error('MSG_BATCH_SIZE not set in env!');
    return;
  }
  const DLQ = process.env.DLQ_URL.trim();
  const MSG_BATCH_SIZE = parseInt(process.env.MSG_BATCH_SIZE);
  const batches: Record[][] = [];
  const errorRecords: Record[] = [];
  //batches[0] = [];
  let batch_no = 0;
  logger.info(`LambdaId ${LambdaId} event: ${JSON.stringify(event)}`);
  for (let i = 0; i < event.Records.length; i++) {
    const record = event.Records[i];

    //console.log('record.kinesis.data', record.kinesis.data);
    const data = Buffer.from(record.kinesis.data, 'base64').toString();
    const userId = record.kinesis.partitionKey;

    //logger.info(`processing record with userId: ${record.kinesis.partitionKey}`);
    //let errorMessage: string[] | string | undefined;
    if (record.eventSourceARN.endsWith(`-hago-stream`)) {
      if (batches[batch_no]) {
        if (batches[batch_no].length >= MSG_BATCH_SIZE) {
          batch_no++;
          batches[batch_no] = [];
        }
      } else {
        batches[batch_no] = [];
      }

      batches[batch_no].push({
        userId,
        data,
      });
      continue;
    } else {
      console.log(`stream name not match, this handler only handle hago stream: ${record.kinesis.partitionKey}`);
      errorRecords.push({
        _message: `stream name not match, this handler only handle hago stream`,
        userId,
        data,
      });
    }
  }

  if (batches.length > 0) {
    batches.forEach(async (batch) => {
      batch.forEach((record) => {
        errorRecords.push({
          _message: `force dlq to test retry batch retry`,
          ...record,
        });
      });
    });

    console.log('starting promise all', batches);
    const allPromises: Promise<any>[] = [];
    batches.forEach((records) => {
      allPromises.push(sendRecord(records));
    });
    const sendRecordStartTime = new Date().getTime();
    const results = await Promise.all(allPromises);

    console.log(allPromises.length + ' parallel requests fired');

    for (let i = 0; i < results.length; i++) {
      const result = results[i];
      if (!Array.isArray(result) && result?.success) {
        logger.info(
          `Success: send records to HA in ${new Date().getTime() - sendRecordStartTime} ms ${JSON.stringify(
            batches[i],
          )}`,
        );
      } else {
        // just put the message directly into errorRecords, hence in DLQ
        batches[i].forEach((record) => {
          errorRecords.push({
            _message: `sendRecords reponse: ${JSON.stringify(result)}`,
            ...record,
          });
        });
      }
    }

    if (errorRecords.length) {
      console.log(LambdaId, 'putting errorRecords to dlq', errorRecords);
      await sqs
        .sendMessage({
          QueueUrl: DLQ,
          MessageBody: JSON.stringify({
            requestId: context.awsRequestId,
            logStreamName: context.logStreamName,
            records: errorRecords,
          }),
        })
        .promise();
    }
  }
};
