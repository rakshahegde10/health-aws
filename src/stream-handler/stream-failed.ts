import { SQSHandler } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import 'source-map-support/register';
import { log, logger } from '../util/als-logger.util';
//import { extractPartitionKey } from '../util/helper.util';
import { KinesisStreamInvocationRecord } from './type/kinesis';
import { Record } from './type/record';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);

const kinesis = new AWS.Kinesis({ apiVersion: '2013-12-02' });
const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });

export const handler: SQSHandler = async (event) => {
  for (const record of event.Records) {
    const { KinesisBatchInfo } = JSON.parse(record.body) as KinesisStreamInvocationRecord;

    const streamName = KinesisBatchInfo.streamArn.split('/')[1];
    const shardIteratorParams: AWS.Kinesis.Types.GetShardIteratorInput = {
      ShardId: KinesisBatchInfo.shardId,
      ShardIteratorType: 'AT_SEQUENCE_NUMBER',
      StreamName: streamName,
      StartingSequenceNumber: KinesisBatchInfo.startSequenceNumber,
    };

    const singleRecord = KinesisBatchInfo.startSequenceNumber === KinesisBatchInfo.endSequenceNumber;

    const shardIteratorOutput = await kinesis.getShardIterator(shardIteratorParams).promise();

    const errorRecords: Record[] = [];
    let shardIterator = shardIteratorOutput.ShardIterator;
    while (shardIterator) {
      const recordsOutput = await kinesis
        .getRecords({
          ShardIterator: shardIterator,
          Limit: singleRecord ? 1 : undefined,
        })
        .promise();

      if (recordsOutput.Records.length === 0) {
        break;
      }

      shardIterator = recordsOutput.NextShardIterator;
      for (const record of recordsOutput.Records) {
        errorRecords.push({
          _message: 'from stream fail queue (retried if retry is available)',
          data: record.Data.toString(),
          userId: record.PartitionKey,
        });
        if (record.SequenceNumber === KinesisBatchInfo.endSequenceNumber) {
          shardIterator = undefined;
          break;
        }
      }
    }

    if (errorRecords.length > 0) {
      const json_records = JSON.stringify(errorRecords);
      logger.info(`Send to DLQ ${json_records}`);
      await sqs
        .sendMessage({
          QueueUrl: process.env.DLQ_URL as string,
          MessageBody: json_records,
        })
        .promise();
    }
  }
};
