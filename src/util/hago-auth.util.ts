//import CryptoJs from 'crypto-js';
import FormData from 'form-data';
import fetch from 'node-fetch';
//import { isNullOrUndefined } from 'util';
import { logger } from './als-logger.util';

let clientAccessTokenResult: any = null;

const HAGO_CARER = 'CARER';

/*const decryptToken = (password: string) => {
  const words = CryptoJs.enc.Base64.parse(password);
  const textString = CryptoJs.enc.Utf8.stringify(words);
  return JSON.parse(textString);
};*/

const getHaGoClientAccessToken = async () => {
  if (
    clientAccessTokenResult &&
    clientAccessTokenResult.access_token_expr &&
    clientAccessTokenResult.access_token_expr > Date.now()
  ) {
    logger.info('ncAppToken not yet expired');
    return clientAccessTokenResult.client_access_token;
  }
  clientAccessTokenResult = null;

  const content = {
    ['client_id']: process.env.HAGO_CLIENT_ID,
    ['client_secret']: process.env.HAGO_CLIENT_SECRET,
  };
  const body = new FormData();
  body.append('content', JSON.stringify(content));
  logger.info('get ncAppToken start');
  logger.info(`get ncAppToken url: ${process.env.HAGO_TOKEN_URL}`);
  let response = null;
  try {
    response = await fetch(process.env.HAGO_TOKEN_URL!, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
      },
      body,
    });
    logger.info(`get ncAppToken response.status:  ${response.status}`);
    if (response.status >= 400) {
      return null;
    }
  } catch (err) {
    logger.error(err);
    return null;
  }
  logger.info('get ncAppToken end');

  const tokenResult = await response.json();

  if (tokenResult.status_code !== '0') {
    return null;
  }

  clientAccessTokenResult = tokenResult.result;
  clientAccessTokenResult['access_token_expr'] = (clientAccessTokenResult.access_token_expr - 60) * 1000;
  return clientAccessTokenResult.client_access_token;
};

export const checkHagoUserToken = async (userId: string, content: string, en_key: string) => {
  const haGoClientAccessToken = await getHaGoClientAccessToken();

  if (!haGoClientAccessToken) {
    console.error('cannot get GoClientAccessToken');
    return false;
  }

  const body = new FormData();

  body.append('content', content);
  body.append('en_key', en_key);

  logger.info(`checkUserToken url: ', ${process.env.HAGO_CHECKTOKEN_URL}`);

  let response = null;
  try {
    response = await fetch(process.env.HAGO_CHECKTOKEN_URL!, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${haGoClientAccessToken}`,
      },
      body,
    });
  } catch (err) {
    logger.error(err);
    return false;
  }
  //logger.debug(`content: ${content}, en_key: ${en_key}`);
  console.log(`content: ${content}`);
  console.log(`en_key: ${en_key}`);
  console.log('HAGO_CHECKTOKEN_URL', process.env.HAGO_CHECKTOKEN_URL);
  console.log('haGoClientAccessToken', 'Bearer ' + haGoClientAccessToken);

  logger.info('checkUserToken response.status: ' + JSON.stringify(response.status));
  if (response.status >= 400) {
    return false;
  }
  const haGoGatewayResponse = await response.json();
  logger.info(`checkUserToken response:  + ${JSON.stringify(haGoGatewayResponse)}`);
  if (haGoGatewayResponse.status_code !== '0') {
    return false;
  }

  let tokenUserId = haGoGatewayResponse.result.user_id;
  if (haGoGatewayResponse.result.hago_user_role === HAGO_CARER && haGoGatewayResponse.result.undercare_user_id) {
    tokenUserId = haGoGatewayResponse.result.undercare_user_id;
  }

  logger.info(`check userId:${userId} == tokenUserId:${tokenUserId} ?`);
  if (!userId && tokenUserId !== userId) {
    logger.error(`userId:${userId} mismtached`);
    return false;
  }

  /*if (!isNullOrUndefined(deviceUuid) && haGoGatewayResponse.result.device_uuid !== deviceUuid) {
    logger.error(`device_uuid:${deviceUuid} mismatched`);
    return false;
  }*/
  return true;
};
