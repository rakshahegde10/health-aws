/* istanbul ignore file */
import log from 'loglevel';

export type AlsMessage = {
  workstationId?: string;
  correlationId?: string;
  transactionId?: string;
  functionId?: string;
  description: string;
  patientIdentity?: string;
  patientLocation?: string;
  content?: string;
};

export type AlsLogMessage = AlsMessage & {
  logDtm: number;
  logType: string;
  userId: string;
  hostName: string;
  locationCd: string;
  projectCode: string;
  encryptMode: string;
};

const toUpperSnake = (str: string): string => str.replace(/[A-Z]/g, (letter) => `_${letter}`).toUpperCase();

const convertMessage = (logType: string, msg: AlsMessage | string): string => {
  const tempMsg = typeof msg === 'string' ? { description: msg } : msg;
  const logMsg: AlsLogMessage = {
    ...tempMsg,
    logDtm: Date.now(),
    logType,
    userId: process.env.AWS_ACCESS_KEY_ID || '',
    hostName: process.env.AWS_LAMBDA_FUNCTION_NAME || '',
    locationCd: process.env.LOCATION_CD || '',
    projectCode: process.env.PROJECT_CODE || '',
    encryptMode: process.env.ENCRYPT_MODE || '',
  };
  const logObj: { [k: string]: any } = {};
  Object.keys(logMsg).forEach((key) => (logObj[toUpperSnake(key)] = (logMsg as any)[key]));
  return JSON.stringify(logObj);
};

const logger = {
  error: (msg: AlsMessage | string) => {
    log.error(convertMessage('ERROR', msg));
  },
  info: (msg: AlsMessage | string) => {
    log.info(convertMessage('INFO', msg));
  },
  debug: (msg: AlsMessage | string) => {
    log.debug(convertMessage('DEBUG', msg));
  },
  trace: (msg: AlsMessage | string) => {
    log.trace(convertMessage('TRACE', msg));
  },
  warn: (msg: AlsMessage | string) => {
    log.warn(convertMessage('WARN', msg));
  },
};

export { log, logger };
