import * as AWS from 'aws-sdk';
import 'source-map-support/register';
import { log, logger } from '../util/als-logger.util';

const searchTime = 20;
const maxRecieveMessageCall = 250;
const DLQ_URL = process.env.DLQ_URL;
const STREAM_FUNCTION = process.env.STREAM_FUNCTION;
log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);
/**
 * This to is to be used by operation team to resend a letter directly to stream lambda in dead letter queue.
 * you can directly use it in aws console or invoke via cli:
 echo -n '{"msgId":"f26ebde8-811b-431e-8dc1-7b15267aa15c"}' | openssl base64;
 aws lambda invoke --function-name myhealth-iot-svc-dev-dlq-retry-function --payload <result from last command> output.json;
 tail/contcat/more output.json;

 */

exports.handler = async (event: any) => {
  var response: string[] = [];
  if (!DLQ_URL) {
    response.push(`queueURL is invalid ${DLQ_URL}`);
    return response;
  }

  if (!STREAM_FUNCTION) {
    response.push(`queueURL is invalid ${STREAM_FUNCTION}`);
    return response;
  }

  AWS.config.update({ region: 'ap-east-1' });
  var sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
  var lambda = new AWS.Lambda();
  const msgId = event.msgId;

  var sqsParams = {
    MaxNumberOfMessages: 10,
    QueueUrl: DLQ_URL,
    VisibilityTimeout: searchTime,
  };

  var lambdaParams: any = {
    FunctionName: STREAM_FUNCTION,
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: { Records: [] },
  };

  let template = {
    kinesis: {
      partitionKey: '<to be filled with sqs data>',
      kinesisSchemaVersion: '1.0',
      data: '<to be filled with sqs data>',
      sequenceNumber: '49545115243490985018280067714973144582180062593244200961',
      approximateArrivalTimestamp: 1428537600,
    },
    eventSource: 'from sqs-retry-records lambda function',
    eventID: '',
    invokeIdentityArn: '',
    eventVersion: '1.0',
    eventName: 'aws:kinesis:record',
    eventSourceARN: 'any-hago-stream',
    awsRegion: 'ap-east-1',
  };

  var loop = 0;

  response.push(
    `looking for messageId ${msgId}, if you want to search other msg id again, please allow 20 s for all messages to be researchable again.`,
  );

  while (loop < maxRecieveMessageCall) {
    loop++;
    const result = await sqs.receiveMessage(sqsParams).promise();
    if (!result.Messages) {
      response.push(`no more records, you may wait ${searchTime} seconds to retry (SQS's VisibilityTimeout)`);
      response.map(($0) => logger.info($0));
      return response;
    }

    const msgs = result.Messages;
    for (let i = 0; i < msgs.length; i++) {
      if (msgs[i].MessageId == msgId) {
        //BINGO!
        console.log('msgs[i]', msgs[i]);
        const bodyStr = msgs[i].Body;
        console.log('bodyStr', bodyStr);
        if (!bodyStr) {
          response.push('no message body!' + JSON.stringify(msgs[i]));
          response.map(($0) => logger.info($0));
          return response;
        }
        const records = JSON.parse(bodyStr).records;
        console.log('records', records);
        console.log('records.length', records.length);
        for (let i = 0; i < records.length; i++) {
          if (records[i].userId) {
            template.kinesis.data = Buffer.from(records[i].data, 'utf8').toString('base64');
            template.kinesis.partitionKey = records[i].userId;
            lambdaParams.Payload.Records.push(template);
          } else {
            response.push(`!!!ERROR!!! no userId for this records`);
            response.map(($0) => logger.info($0));
            return response;
          }
        }
        console.log(lambdaParams.Payload);
        lambdaParams.Payload = JSON.stringify(lambdaParams.Payload);
        response.push(`${STREAM_FUNCTION} invoked. Please check cloudwatch log`);
        const invoke_result = await lambda.invoke(lambdaParams).promise();
        response.push(`result status code ${invoke_result?.StatusCode?.toString()}`);
        response.map(($0) => logger.info($0));
        return response;
      } else {
        //response.push('call ' + loop + ' i ' + i + '/' + msgs.length + ' XXX ' + msgs[i].MessageId);
      }
    }
  }
  response.push(
    `error called sqs receveMessage ${maxRecieveMessageCall} times still cannot find messageId ${msgId}, check messageId and sqs name`,
  );
  response.map(($0) => logger.info($0));
  return response;
};
