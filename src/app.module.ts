import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
//import { DynamooseModule } from 'nestjs-dynamoose';
import { LoggerModule } from './logger/logger.module';
import { MeasurementModule } from './measurement/measurement.module';

@Module({
  imports: [ConfigModule.forRoot(), LoggerModule, MeasurementModule],
})
export class AppModule {}
