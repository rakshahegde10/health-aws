import { APIGatewayProxyHandler } from 'aws-lambda';
import 'source-map-support/register';

//This function catch sendMessage from stream lambda, for benchmark performance
export const hello: APIGatewayProxyHandler = async () => {
  await delay(500);

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        success: true,
      },
      null,
      2,
    ),
  };
};

async function delay(s: any) {
  return new Promise((resolve) => {
    setTimeout(resolve, s);
  });
}
