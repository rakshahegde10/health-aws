# My Health API

## Description

My Health RESTful API for Vendor

[API Documentation](https://doc-sed-apidoc-ft.cldpaast71.server.ha.org.hk/)

## Technologies

- [AWS Lambda](https://aws.amazon.com/lambda)
- [Serverless](https://serverless.com/framework/docs/providers/aws/)
- [NestJS](https://docs.nestjs.com/)

## Usage

```bash
npm install
serverless deploy
```

## Setup AWS Credentials

1. [Sign up for an AWS account](https://serverless.com/framework/docs/providers/aws/guide/credentials#sign-up-for-an-aws-account)
2. Login to your AWS account and go to the **Identity & Access Management (IAM)** page.
3. Click on **Users** and then **Add user**. Enter a name in the first field to remind you this User is related to the Serverless Framework, like `serverless-admin`. Enable **Programmatic access** by clicking the checkbox. Click **Next** to go through to the Permissions page. Click on **Attach existing policies directly**. Search for and select **AdministratorAccess** then click **Next: Review**. Check to make sure everything looks good and click **Create user**.
4. View and copy the **API Key & Secret** to a temporary place. You'll need it in the next step.

## Setup Development Environment

Install AWS CLI

- Windows: `choco install awscli`
- MacOS: `brew install awscli`

Config AWS CLI

```bash
$ aws configure

AWS Access Key ID [****************TKYQ]:
AWS Secret Access Key [****************yNO2]:
Default region name [None]:
Default output format [None]:
```

> Please enter your **AWS Access Key ID** and **AWS Secret Access Key**

## Deployment

Please first create the following kms key with alias

- myhealth-iot-svc-kinesis-kms
- myhealth-iot-svc-dev-sqs-kms
- myhealth-iot-svc-dev-s3-kms

> AWS CLI Sample :
>
> ```
> $ aws kms create-key
> {
>    "KeyMetadata": {
>        "AWSAccountId": "836175332282",
>        "KeyId": "cc4da34e-4050-41e0-86db-a6d77b903515",
>    ...
> }
>
> $ aws kms create-alias \
>     --alias-name alias/cms-iot-svc-dev-dynamodb-kms \
>     --target-key-id cc4da34e-4050-41e0-86db-a6d77b903515
> ```

Deploy to AWS

```bash
# deploy to AWS
$ npm run deploy
```

## Unit Testing

```bash
# run unit test
$ npm test

# run unit test with coverage
$ npm run test:cov
```

## Cognito Client Credentials Grant

Please obtain the `Access Token` for the RESTful Endpoint

```
curl -X POST 'https://cms-pos.auth.ap-southeast-1.amazoncognito.com/oauth2/token' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Authorization: Basic BASE64(CLIENT_ID:CLIENT_SECRET)' \
  --data-urlencode 'grant_type=client_credentials'
```

> The Authorization header for this request is set as “Basic BASE64(CLIENT_ID:CLIENT_SECRET)“,
> where BASE64(CLIENT_ID:CLIENT_SECRET) is the base64 representation of the app client ID and app client secret, concatenated with a colon.

## RESTful Endpoint Test

curl -X POST 'https://<from ur deployment>.execute-api.ap-east-1.amazonaws.com/dev/v1/measurement'
-H 'Content-Type: application/json'
-H 'Authorization: Bearer <Access Token>'
--data-raw '{ "userId": "123456789", data:"dummy" }'

curl -X GET 'https://wwpzlynvs7.execute-api.ap-east-1.amazonaws.com/dev/v1/registration/HKT/LYM/rn00000001'
-H 'Content-Type: application/json'
-H 'Authorization: Bearer <Access Token>'
